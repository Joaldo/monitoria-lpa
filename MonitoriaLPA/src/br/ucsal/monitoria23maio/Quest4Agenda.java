package br.ucsal.monitoria23maio;

import java.util.Scanner;

public class Quest4Agenda {

	Scanner sc = new Scanner(System.in);

	public String nome = " ";
	private String telefone = " ";
	private Integer anoNascimento;

	String[] nomes = new String[100];
	String[] telefones = new String[100];
	Integer[] anoNascimentos = new Integer[100];

	public void incluirContato() {
		boolean f = true;
		for (int cont = 0; f == true; cont++) {
			System.out.println("Nome: ");
			nome = sc.nextLine();
			nomes[cont] = nome;
			System.out.println("Telefone: ");
			telefone = sc.nextLine();
			telefones[cont] = telefone;
			System.out.println("Ano de Nascimento: ");
			anoNascimento = sc.nextInt();
			anoNascimentos[cont] = anoNascimento;
			f = false;
		}

	}

	public void excluirContato() {
		boolean f = true;
		for (int cont = 0; f == true; cont++) {

			nomes[cont] = "Nome excluido";
			telefones[cont] = "Telefone excluido";
			anoNascimentos[cont] = null;
			System.out.println(nomes[cont]);
			System.out.println(telefones[cont]);
			System.out.println(anoNascimentos[cont]);

			f = false;
		}
	}

	public void listarContato() {
		boolean f = true;
		for (int cont = 0; f == true; cont++) {
			System.out.println("Nome: " + nomes[cont]);
			System.out.println("Telefone: " + telefones[cont]);
			System.out.println("Ano de nascimento: " + anoNascimentos[cont]);

			f = false;
		}
	}

	public void mensagem() {

		System.out.print("\n");
		System.out.println("1 - Incluir");
		System.out.println("2 - Excluir");
		System.out.println("3 - Listar");
		System.out.println("4 - Sair da agenda");

	}

}
