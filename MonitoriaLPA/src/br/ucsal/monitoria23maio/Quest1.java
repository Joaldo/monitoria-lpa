package br.ucsal.monitoria23maio;

public class Quest1 {

	public static void main(String[] args) {
		int[] vetorA = new int[15];
		vetorA[0] = 35;
		vetorA[1] = 65;
		vetorA[2] = 41;
		vetorA[3] = 55;
		vetorA[4] = 19;
		vetorA[5] = 54;
		vetorA[6] = 50;
		vetorA[7] = 36;
		vetorA[8] = 9;
		vetorA[9] = 78;
		vetorA[10] = 92;
		vetorA[11] = 86;
		vetorA[12] = 34;
		vetorA[13] = 11;
		vetorA[14] = 5;

		int soma = 0, somaPar = 0, cont = 0;

		for (int i = 0; i < vetorA.length; i++) {
			soma += vetorA[i];
			System.out.print((i + 1) + "�valor = " + vetorA[i] + ". ");
		}
		for (int i = 0; i < vetorA.length; i++) {

			if (vetorA[i] % 2 == 0) {
				somaPar += vetorA[i];
				System.out.print("\nPosi��o no vetor " + cont + " valor " + vetorA[i] + " ");
			}
			cont++;
		}

		System.out.println("\nSoma de todos os valores: " + soma);
		System.out.println("Soma de todos os valores pares: " + somaPar);

	}

}
