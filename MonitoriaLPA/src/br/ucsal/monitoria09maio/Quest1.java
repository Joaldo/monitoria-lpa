package br.ucsal.monitoria09maio;

import javax.swing.JOptionPane;

public class Quest1 {
	public static void main(String[] args) {
		int a, b;
		a = informeValor(1);
		b = informeValor(2);

		int z = adicao(a, b);
		imprimir("Adi��o", z);
		z = subtracao(a, b);
		imprimir("Subtra��o", z);
		double w = multiplicacao(a, b);
		imprimir("Multiplica��o", z);
		w = divisao(a, b);
		imprimir("Divis�o", w);
	}

	static int informeValor(int vlr) {

		return Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o " + vlr + "� valor"));
	}

	public static int adicao(int a, int b) {
		return a + b;
	}

	public static int subtracao(int a, int b) {
		int sub;
		if (a < b) {
			sub = b - a;
		} else {
			sub = a - b;
		}
		return sub;
	}

	public static int multiplicacao(int a, int b) {
		return a * b;
	}

	public static double divisao(double a, double b) {
		return a / b;
	}

	public static void imprimir(String operacao, int res) {
		JOptionPane.showMessageDialog(null, operacao + " = " + res);
	}

	public static void imprimir(String operacao, double res) {
		JOptionPane.showMessageDialog(null, operacao + " = " + res);
	}

}
