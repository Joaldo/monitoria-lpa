package br.ucsal.monitoria09maio;

import java.util.Scanner;

public class Quest3 {
	static Scanner sc = new Scanner(System.in);
	static double saldo;
	static String conta;

	public static void main(String[] args) {
		System.out.println("Conta: ");
		conta = sc.nextLine();
		double valor = 0;
		boolean condicao = true;
		int opcao;
		while (condicao == true) {

			System.out.println("Escolha uma op��o: \n1 - Depositar dinheiro" + "\n2 - Consultar saldo"
					+ "\n3 - Fazer saque" + "\n4 - Sair");
			opcao = sc.nextInt();
			switch (opcao) {
			case 1:
				System.out.println("Valor a ser depositado: ");
				valor = informarValor(0);
				System.out.println("Valor depositado R$: " + valor + "\n");
				saldo = depositarDinheiro(valor);
				break;
			case 2:
				System.out.println("Seu saldo atual � R$: " + saldo + "\n");

				break;
			case 3:
				System.out.println("Valor a ser sacado: ");
				valor = informarValor(0);
				if(saldo < valor) {
					System.out.println("Saldo insuficiente.\n");
				}else {
					System.out.println("Valor sacado R$: " + valor + "\n");
					saldo = sacarDinheiro(valor);
				}
				break;
			case 4:
				condicao = false;
				System.out.println("At� a proxima. Conta: " + conta);
				break;
			default:
				System.out.println("Op��o inv�lida!\n");
				break;
			}
		}
	}

	public static double informarValor(double valor) {

		return valor = sc.nextDouble();
	}

	public static double depositarDinheiro(double deposito) {
		double saldoParcial = 0;
		saldoParcial = saldo + deposito;
		return saldoParcial;
	}

	public static double sacarDinheiro(double saque) {
		double saldoParcial = 0;
		if (saldo > saque) {
			saldoParcial = saldo - saque;
		} else {
			System.out.println("ATEN��O!!!"
					+ "\nVoc� esta tentando sacar um valor maior que o seu saldo.\n");
		}
		return saldoParcial;
	}
}
