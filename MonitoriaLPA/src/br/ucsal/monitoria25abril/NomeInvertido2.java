package br.ucsal.monitoria25abril;

import javax.swing.JOptionPane;

public class NomeInvertido2 {
	public static void main(String[] args) {
		String nome = JOptionPane.showInputDialog("Informe seu nome");
		int posicao = 0;
		for (int i = 0; i < nome.length(); i++) {
			if (nome.substring(i, i + 1).equals(" ")) {
				posicao = i;
			}
		}
		JOptionPane.showMessageDialog(null, nome.substring(posicao + 1, nome.length()).toUpperCase() + ", "
				+ nome.substring(0, 1).toUpperCase() + nome.substring(1, posicao).toLowerCase());
		JOptionPane.showMessageDialog(null, nome.substring(0, 1).toUpperCase() + (". ")
				+ nome.substring(posicao + 1, posicao + 2).toUpperCase() + ("."));
	}

}
