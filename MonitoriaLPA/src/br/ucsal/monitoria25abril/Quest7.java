package br.ucsal.monitoria25abril;

import java.util.Random;
import java.util.Scanner;

public class Quest7 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Random rd = new Random();

		int num = rd.nextInt(100) + 1;
		int tentativa, cont = 0;
		System.out.println("Qual o seu chute?");
		tentativa = sc.nextInt();
		while (num != tentativa) {
			if (tentativa > num) {
				System.out.println("Tenta chutar um n�mero mais baixo.");
				System.out.println("Qual o seu chute?");
				tentativa = sc.nextInt();
				cont++;
			} else if (tentativa < num) {
				System.out.println("Tenta chutar um n�mero mais alto.");
				System.out.println("Qual o seu chute?");
				tentativa = sc.nextInt();
				cont++;
			}
		}
		if (num == tentativa) {
			System.out.println("Acertou MISERAVE " + (cont + 1) + " tentativas.");
		}
		sc.close();
	}
}
