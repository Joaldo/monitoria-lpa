package br.ucsal.monitoria25abril;

import java.util.Scanner;

public class Quest3 {

	public static void main(String[] args) {

		int num1, num2;
		String txt = "";

		for (int i = 0; i < 5; i++) {
			System.out.println("Informe 2 valores");
			System.out.println("Primeiro: ");
			num1 = new Scanner(System.in).nextInt();
			System.out.println("Segundo: ");
			num2 = new Scanner(System.in).nextInt();
			for (int j = num1; j <= num2; j++) {
				if (j % 2 != 0) {
					txt += j + 1 + " ";
				}
			}
			System.out.print(num1 + " " + txt + "\n");
			txt = " ";
		}
	}
}
