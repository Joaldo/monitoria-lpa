package br.ucsal.monitoria25abril;

import java.util.Scanner;

public class Quest4 {

	public static void main(String[] args) {

		int valor;
		Scanner sc = new Scanner(System.in);
		System.out.println("Insira a quantidade em R$:");
		valor = sc.nextInt();

		if (valor / 100 > 0) {
			System.out.println(valor / 100 + " nota(s) de R$ 100");
		}
		valor %= 100;
		if (valor / 50 > 0) {
			System.out.println(valor / 50 + " nota(s) de R$ 50");
		}
		valor %= 50;
		if (valor / 10 > 0) {
			System.out.println(valor / 10 + " nota(s) de R$ 10");
		}
		valor %= 10;
		if (valor / 5 > 0) {
			System.out.println(valor / 5 + " nota(s) de R$ 5");
		}
		valor %= 5;
		if (valor / 1 > 0) {
			System.out.println(valor / 1 + " nota(s) de R$ 1");
		}
		sc.close();
	}

}
