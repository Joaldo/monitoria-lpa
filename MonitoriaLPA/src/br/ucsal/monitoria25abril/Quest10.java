package br.ucsal.monitoria25abril;

import java.util.Scanner;

public class Quest10 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Integer qtdPacientes = 0, somaIdadeHomens = 0, qtdHomens = 0, qtdAlturaMulheres = 0, qtdMulheres = 0;
		Integer qtdPesoIdade = 0, mediaIdadeHomens, fim = 1, sexo, idade, maiorIdade;
		float peso, altura, menorAltura;
		String nomeMaisVelho = "", nome;
		String nomeMaisBaixa = "N�o foi cadastrado nenhuma mulher";

		System.out.println("Digite o nome");
		nome = sc.next();
		System.out.println("Digite sexo: 1 - Feminino\n" + "             2 - Masculino");
		sexo = sc.nextInt();
		while (sexo < 1 || sexo > 2) {
			System.out.println("Op��o Inv�lida");
			System.out.println("Digite sexo: 1 - Feminino\n" + "             2 - Masculino");
			sexo = sc.nextInt();
		}
		System.out.println("Digite o seu peso(Kg): ");
		peso = sc.nextFloat();
		System.out.println("Digite a sua altura(m): ");
		altura = sc.nextFloat();
		System.out.println("Digite sua idade: ");
		idade = sc.nextInt();

		qtdPacientes++;

		if (idade >= 18 && idade <= 25) {
			qtdPesoIdade++;
		}
		maiorIdade = idade;
		nomeMaisVelho = nome;
		menorAltura = altura;

		if (sexo == 1) {
			if (altura >= 1.60 && altura <= 1.70 && peso > 70) {
				qtdAlturaMulheres++;
			}
			nomeMaisBaixa = nome;
		} else if (sexo == 2) {
			qtdHomens++;
			somaIdadeHomens += idade;
		}
		System.out.println("Deseja sair? 1 - N�o\n" + "             2 - Sim");
		fim = sc.nextInt();

		while (fim < 1 || fim > 2) {
			System.out.println("Op��o inv�lida");
			System.out.println("Deseja sair? 1 - N�o\n" + "             2 - Sim");
			fim = sc.nextInt();
		}
		if (fim == 2) {
			System.out.println("FLW!!!");
		}

		while (fim == 1) {
			System.out.println("Digite o nome");
			nome = sc.next();
			System.out.println("Digite sexo: 1 - Feminino\n" + "             2 - Masculino");
			sexo = sc.nextInt();
			while (sexo < 1 || sexo > 2) {
				System.out.println("Op��o Inv�lida");
				System.out.println("Digite sexo: 1 - Feminino\n" + "             2 - Masculino");
				sexo = sc.nextInt();
			}
			System.out.println("Digite o seu peso(Kg): ");
			peso = sc.nextFloat();
			System.out.println("Digite a sua altura(m): ");
			altura = sc.nextFloat();
			System.out.println("Digite sua idade: ");
			idade = sc.nextInt();

			qtdPacientes++;

			if (idade >= 18 && idade <= 25) {
				qtdPesoIdade++;
			}
			if (maiorIdade < idade) {
				maiorIdade = idade;
				nomeMaisVelho = nome;
			}
			if (sexo == 1) {
				if (altura >= 1.60 && altura <= 1.70 && peso > 70) {
					qtdAlturaMulheres++;
				}
				if (menorAltura > altura) {
					menorAltura = altura;
					nomeMaisBaixa = nome;
				}
			} else if (sexo == 2) {
				qtdHomens++;
				somaIdadeHomens += idade;
			}
			System.out.println("Deseja sair? 1 - N�o\n" + "             2 - Sim");
			fim = sc.nextInt();

			while (fim < 1 || fim > 2) {
				System.out.println("Op��o inv�lida");
				System.out.println("Deseja sair? 1 - N�o\n" + "             2 - Sim");
				fim = sc.nextInt();
			}
			if (fim == 2) {
				System.out.println("FLW!!!");
			}

		}
		if (qtdHomens == 0) {
			mediaIdadeHomens = 0;
		} else {
			mediaIdadeHomens = somaIdadeHomens / qtdHomens;
		}
		qtdMulheres = qtdPacientes - qtdHomens;

		System.out.println("A quantidade de pacientes �: " + qtdPacientes + " paciente(s).");
		System.out.println("A m�dia de idade dos homens �: " + mediaIdadeHomens + " anos.");
		System.out.println("A quantidade de mulheres com altura entre 160 e 1,70 e peso superior a 70Kg �: "
				+ qtdAlturaMulheres + " mulhere(s).");
		System.out.println("A quantidade de pesso entre 18 e 25 anos �: " + qtdPesoIdade + " pessoa(s).");
		System.out.println("O nome do paci�nte mais velho �: " + nomeMaisVelho);
		System.out.println("O nome da mulher mais baixa �: " + nomeMaisBaixa);
	}

}
