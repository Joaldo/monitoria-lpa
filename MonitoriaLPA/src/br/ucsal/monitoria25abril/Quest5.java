package br.ucsal.monitoria25abril;

import java.util.Scanner;

public class Quest5 {
	public static void main(String[] args) {

		int matricula, cont1 = 0, cont2 = 0;
		float altura, maiorAlt = 0, menorAlt = 1000;

		for (int i = 0; i < 5; i++) {

			System.out.println("Informe o numero de matricula");
			matricula = new Scanner(System.in).nextInt();
			System.out.println("Informe a altura");
			altura = new Scanner(System.in).nextFloat();

			if (altura > maiorAlt) {

				maiorAlt = altura;
				cont1 = matricula;

			}
			if (altura < menorAlt) {

				menorAlt = altura;
				cont2 = matricula;
			}
		}

		System.out.println("Maior altura= " + maiorAlt + " Matricula do mais alto = " + cont1);
		System.out.println("Menor altura = " + menorAlt + " Matricula do mais baixo = " + cont2);
	}
}
