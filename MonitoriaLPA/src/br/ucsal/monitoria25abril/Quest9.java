package br.ucsal.monitoria25abril;

import java.util.Scanner;

public class Quest9 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int intervalo = 0;
		System.out.println("Digite o limete de n�meros a ser verificado: ");
		intervalo = sc.nextInt();

        for (int i = 1; i < intervalo; i++) {
            boolean primo = true;
            for (int j = 2; j <= i; j++) {
                if (((i % j) == 0) && (j != i)) {
                    primo = false;
                    break;
                }
            }

            if (primo) {
                System.out.println(i + " � Primo!");
            } 
        }
    }
}

