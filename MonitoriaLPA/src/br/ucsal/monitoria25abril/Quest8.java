package br.ucsal.monitoria25abril;

import java.util.Scanner;

public class Quest8 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int fatorial = 1;
		int valor = 0;
		System.out.println("Numero que deseja obter o fatorial: ");
		valor = sc.nextInt();
		
		for (int i = 1; i <= valor; i++) {
			fatorial *= i;
		}
		System.out.println("O fatorial de " + valor + " � " + fatorial);
		System.out.println("\nApartir do fatorial de 17 o eclipse buga o n�mero, por ser n�mero muito grande");
	}
}
