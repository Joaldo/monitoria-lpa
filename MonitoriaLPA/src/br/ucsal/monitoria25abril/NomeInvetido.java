package br.ucsal.monitoria25abril;

import javax.swing.JOptionPane;

public class NomeInvetido {
	public static void main(String[] args) {

		String frase = JOptionPane.showInputDialog("Insira a frase");
		String invertido = "";
		for (int i = (frase.length() - 1); i >= 0; i--) {
			invertido += frase.charAt(i);
		}
		JOptionPane.showMessageDialog(null, invertido);
	}

}
