package br.ucsal.monitoria25abril;

import java.util.Scanner;

public class Quest1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String mes;
		System.out.println("M�s que deseja: ");
		mes = sc.nextLine().toLowerCase();

		if (mes.equals("janeiro") || mes.equals("fevereiro") || mes.equals("mar�o")) {
			System.out.println(mes + " � Ver�o");
		} else if (mes.equals("abril") || mes.equals("maio") || mes.equals("junho")) {
			System.out.println(mes + " � Outono");
		} else if (mes.equals("julho") || mes.equals("agosto") || mes.equals("setembro")) {
			System.out.println(mes + " � Inverno");
		} else if (mes.equals("outubro") || mes.equals("novembro") || mes.equals("dezembro")) {
			System.out.println(mes + " � Primavera");
		} else {
			System.out.println("M�s inv�lido");

		}
	}
}
