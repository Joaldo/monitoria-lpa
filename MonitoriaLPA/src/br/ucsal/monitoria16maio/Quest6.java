package br.ucsal.monitoria16maio;

import java.util.Scanner;

public class Quest6 {

	static String letras[] = { "m", "u", "n", "d", "o"};
	static Scanner sc = new Scanner(System.in);

	public static void main(String args[]) {
		System.out.println("A palavra tem " + letras.length + " letras");
		jogarForca();
	}

	public static void jogarForca() {

		boolean verificador = true;
		int acertos = 0, erros = 0, contador = 0;
		String digito;
		String letrasDigitadas[] = new String[20];

		while (verificador == true) {

			System.out.println("Digite a letra:");
			digito = sc.next();
			letrasDigitadas[contador] = digito;
			contador++;

			if (!digito.equals(letras[0]) && !digito.equals(letras[1]) && !digito.equals(letras[2]) && !digito.equals(letras[3])
					&& !digito.equals(letras[4])) {
				System.out.println("Letra inexistente.");
				erros++;
				System.out.println("Voc� tem " + (8 - erros) + " tentativas");
				if (erros == 8) {
					System.out.println("Voce perdeu.");
					verificador = false;
				}
			} else {
				System.out.println("Letra correta.");
				acertos++;
				if (acertos == 5) {
					System.out
							.print("A palavra �: " + letras[0] + letras[1] + letras[2] + letras[3] + letras[4] + "\n");
					System.out.println("Voce venceu. Com " + contador + " jogadas");
					verificador = false;
				}
			}
		}
		sc.close();
	}
}
