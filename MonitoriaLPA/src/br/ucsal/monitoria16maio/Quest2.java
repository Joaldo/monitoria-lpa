package br.ucsal.monitoria16maio;

import javax.swing.JOptionPane;

public class Quest2 {

	public static void main(String[] args) {

		JOptionPane.showMessageDialog(null, recebeData(0, 0));

	}

	public static String recebeData(int mes, int dia) {

		dia = Integer.parseInt(JOptionPane.showInputDialog(null, "Dia em que voc� nasceu: "));
		mes = Integer.parseInt(JOptionPane.showInputDialog(null, "M�s do seu nascimento: "));

		if (mes == 3 && dia > 21 && dia < 31 || mes == 4 && dia < 20 && dia < 30) {
			return "�ries";			
		}else if (mes == 4 && dia > 21 && dia < 30 || mes == 5 && dia < 20) {
			return "Touro";			
		}else if (mes == 5 && dia > 21  && dia < 31|| mes == 6 && dia < 20) {
			return "G�meos";			
		}else if (mes == 6 && dia > 21  && dia < 30|| mes == 7 && dia < 22) {
			return "Canc�r";			
		}else if (mes == 7 && dia > 23  && dia < 31|| mes == 8 && dia < 22) {
			return "Le�o";			
		}else if (mes == 8 && dia > 23  && dia < 31|| mes == 9 && dia < 22) {
			return "Virgem";			
		}else if (mes == 9 && dia > 23  && dia < 30|| mes == 10 && dia < 22) {
			return "Libra";			
		}else if (mes == 10 && dia > 23  && dia < 31|| mes == 11 && dia < 21) {
			return "Escorpi�o";			
		}else if (mes == 11 && dia > 22  && dia < 30|| mes == 12 && dia < 21) {
			return "Sargit�rio";			
		}else if (mes == 12 && dia > 22  && dia < 31|| mes == 1 && dia < 20) {
			return "Capricornio";			
		}else if (mes == 1 && dia > 21  && dia < 31|| mes == 2 && dia < 18) {
			return "Aqu�rio";			
		}else if (mes == 2 && dia > 19  && dia < 28|| mes == 3 && dia < 20) {
			return "Peixes";			
		}else {
			return "M�s ou dia inv�lido";
		}

	}
}
