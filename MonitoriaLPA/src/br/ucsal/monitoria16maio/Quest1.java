package br.ucsal.monitoria16maio;

import javax.swing.JOptionPane;

public class Quest1 {

	public static void main(String[] args) {
		
		int opcao = Integer.parseInt(JOptionPane.showInputDialog("Escolha um das op��es:"
				+ "\n1 - Adi��o"
				+ "\n2 - Subtra��o"
				+ "\n3 - Multiplica��o"
				+ "\n4 - Divis�o"));
		double num1 = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite o primeiro numero: "));
		double num2 = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite o segundo numero: "));
		
		switch(opcao) {
		case 1:
			JOptionPane.showMessageDialog(null, adicao(num1, num2));
			break;
		case 2:
			JOptionPane.showMessageDialog(null, subtracao(num1, num2));
			break;
		case 3: 
			JOptionPane.showMessageDialog(null, multiplicacao(num1, num2));
			break;
		case 4:
			JOptionPane.showMessageDialog(null, divisao(num1, num2));
			break;
		default:
			JOptionPane.showMessageDialog(null, "Op��o invalida");
			break;
		}
		

	}

	public static double adicao(double num1, double num2) {

		return num1 + num2;
	}

	public static double subtracao(double num1, double num2) {

		return num1 - num2;
	}

	public static double multiplicacao(double num1, double num2) {

		return num1 * num2;
	}

	public static double divisao(double num1, double num2) {

		return num1 / num2;
	}

}
