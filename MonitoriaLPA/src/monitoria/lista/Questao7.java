package monitoria.lista;

public class Questao7 {

	public static void main(String[] args) {
		double valor1 = 2.5, valor2 = 4.5, proximo;
	
		int i = 1;
		System.out.println(i + ": " + valor1);
		i++;
		System.out.println(i + ": " + valor2);

		double media = 0, m1 = 0, m2 = 0;

		double total = valor1 + valor2;
		do {
			proximo = ((valor1 + valor2) / 2) + 10;
			System.out.println(i + "�: " + proximo);
			valor1 = valor2;
			valor2 = proximo;
			total = total + proximo;
			i++;
			if (i == 4) {
				m1 = proximo;
			}
			if (i == 8) {
				m2 = proximo;
			}
		} while (i <= 12);

		media = (m2 + m1) / 2;
		System.out.println("A m�dia dos termos 4 e 8 �: " + media);
		System.out.println("A soma de todos os elementos �: " + total);
	}

}
