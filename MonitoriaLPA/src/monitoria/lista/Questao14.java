/*
	Resolu�o da quest�o 01 da prova abrangendo todas a as possibilidades 
	Sem o DATE fica um negocio enorme e sem sentido, mas � uma das formas de se resolver
*/
package monitoria.lista;

import java.util.Scanner;

public class Questao14 {

	public static void main(String[] args) {

		int anosBisextos, anoNascimento, nascimento, mes, dia, cont = 0, compara = 0, atual = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Ano atual: ");
		atual = sc.nextInt();
		System.out.println("Seu ano de nascimento");
		anoNascimento = sc.nextInt();
		nascimento = anoNascimento;
		compara += anoNascimento;
		int idade = atual - anoNascimento;

		while (atual > anoNascimento) {
			if (anoNascimento % 4 == 0 && anoNascimento % 100 != 0 || anoNascimento % 4 == 0) {
				cont++;
			}
			anoNascimento++;
		}
		anosBisextos = cont;
		anosBisextos *= 366;
		System.out.println("Desde o seu nascimento ocorreram " + cont + " ano(s) bisexto(s)");
		System.out.println("Voc� tem " + idade + " anos de vida");

		System.out.println("Mes que voc� nasceu: ");
		mes = sc.nextInt();
		System.out.println("Dia do mes " + mes + " que voc� nasceu?");
		dia = sc.nextInt();
		compara = (atual - compara) - cont;
		compara = compara * 355;
		
		int acumulaDia = 0;

		if (nascimento % 4 == 0 && nascimento % 100 != 0 || nascimento % 4 == 0) {
			if (mes == 1) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 31;
			if (mes == 2) {
				if (dia >= 1 && dia <= 28) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia invalido");
				}
			}
			acumulaDia += 28;
			if (mes == 3) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 31;
			if (mes == 4) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 5) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 6) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 7) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 31;
			if (mes == 8) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 9) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 10) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 31;
			if (mes == 11) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 12) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
		} else {
			if (mes == 1) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
				acumulaDia += 31;
			}
			if (mes == 2) {
				if (dia >= 1 && dia <= 29) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia invalido");
				}
			}
			acumulaDia += 29;
			if (mes == 3) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 31;
			if (mes == 4) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 5) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 6) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 7) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 31;
			if (mes == 8) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 9) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 10) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 31;
			if (mes == 11) {
				if (dia >= 1 && dia <= 30) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
			acumulaDia += 30;
			if (mes == 12) {
				if (dia >= 1 && dia <= 31) {
					int totalDias = anosBisextos + compara + (acumulaDia + dia);
					System.out.println("Voc� tem " + totalDias + " dias de vida.");
				} else {
					System.out.println("Dia inv�lido");
				}
			}
		}
	}
}
