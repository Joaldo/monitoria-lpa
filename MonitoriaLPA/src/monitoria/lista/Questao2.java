package monitoria.lista;

import java.util.Scanner;

public class Questao2 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		float nota1, nota2, nota3, media;
		
		System.out.println("Primeira Nota: ");
		nota1 = sc.nextFloat();
		System.out.println("Segunda Nota: ");
		nota2 = sc.nextFloat();
		System.out.println("Terceira Nota: ");
		nota3 = sc.nextFloat();
		media = (nota1 + nota2 + nota3) / 3;
		
		if(media >= 7.0) {
			System.out.println("Sua m�dia final � " + media + " Aprovado");
		}else if(media <= 3.5){
			System.out.println("Sua m�dia final � " + media + " Reprovado");
		}else {
			System.out.println("Sua m�dia final � " + media + ". Voc� tera que fazer a prova final");
			System.out.println("\nSua nota da prova final: ");
			float provaFinal = sc.nextFloat();
			if(provaFinal >= 6) {
				System.out.println("Aprovado na prova final");
			}else {
				System.out.println("Reprovado na prova final");
			}
		}
	}

}
