package monitoria.lista;

import java.util.Scanner;

public class Questao3 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int valor1, valor2, valor3;
		System.out.println("Informe 3 valores entre 20 e 500!");
		System.out.println("Primeiro valor:");
		valor1 = sc.nextInt();
		System.out.println("Segundo valor:");
		valor2 = sc.nextInt();
		System.out.println("Terceiro valor:");
		valor3 = sc.nextInt();

		int somaTotal = valor1 + valor2 + valor3;
		if (valor1 >= 20 && valor1 <= 500 && valor2 >= 20 && valor2 <= 500 && valor3 >= 20 && valor3 <= 500) {
			if (somaTotal % 4 == 0 && somaTotal % 5 == 0) {
				System.out.println("A soma dos 3 valores � " + somaTotal + " ele � um n�mero divisel por 4 e 5");
			} else if (somaTotal % 2 == 0 && somaTotal % 6 == 0) {
				System.out.println("A soma dos 3 valores � " + somaTotal + " ele � um n�mero divisel por 2 e 6");
			} else if (somaTotal % 3 == 0) {
				System.out.println("A soma dos 3 valores � " + somaTotal + " ele � n�mero divisel por 3");
			} else {
				System.out.println("N�mero n�o � divisivel por nunhuma das opera��es");
			}
		} else {
			System.out.println("Todos os valores devem estar dentro do intervalo informado, execute novamente");
		}
	}
}
