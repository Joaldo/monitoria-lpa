package monitoria.lista;

import java.util.Scanner;

public class Questao4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double ladoA, ladoB, ladoC;
		System.out.println("Primeiro lado:");
		ladoA = sc.nextDouble();
		System.out.println("Segundo lado:");
		ladoB = sc.nextDouble();
		System.out.println("Terceiro lado:");
		ladoC = sc.nextDouble();
		
		if(ladoA > (ladoB - ladoC) && ladoA < (ladoB + ladoC) || ladoB > (ladoA - ladoC) && ladoB < (ladoA + ladoC) || ladoC > (ladoA - ladoB) && ladoC < (ladoA + ladoB)) {
			System.out.println("Os valores podem formar um tri�ngulo.");
			if(ladoA == ladoB && ladoA == ladoC && ladoB == ladoC) {
				System.out.println("Trin�ngulo equil�tero.");
			}else if(ladoA == ladoB && ladoA != ladoC || ladoB == ladoC && ladoB != ladoA) {
				System.out.println("Tri�ngulo is�sceles.");
			}else if(ladoA != ladoB && ladoA != ladoC && ladoB != ladoC){
				System.out.println("Tri�ngulo escaleno.");
			}
		}else {
			System.out.println("OS valores n�o correspondem a um tri�ngulo.");
		}
		
	}

}
