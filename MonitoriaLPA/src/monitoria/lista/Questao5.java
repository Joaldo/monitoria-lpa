package monitoria.lista;

import java.util.Scanner;

public class Questao5 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int valor1, valor2, valor3, aux;
		System.out.println("Informe 3 valores em um intervalo entre 10 e 500.");
		System.out.println("Primeiro valor:");
		valor1 = sc.nextInt();
		System.out.println("Segundo valor:");
		valor2 = sc.nextInt();
		System.out.println("Terceiro valor:");
		valor3 = sc.nextInt();

		if (valor1 > valor2) {
			aux = valor1;
			valor1 = valor2;
			valor2 = aux;
		}
		if (valor2 > valor3) {
			aux = valor3;
			valor3 = valor2;
			valor2 = aux;
		}
		if (valor1 > valor2) {
			aux = valor1;
			valor1 = valor2;
			valor2 = aux;
		}

		System.out.println(valor1 + ", " + valor2 + " e " + valor3 + ".");

		if (valor1 < 10 || valor1 > 500) {
			System.out.println("O n�mero " + valor1 + " est� fora do intervalo.");
		}
		if (valor2 < 10 || valor2 > 500) {
			System.out.println("O n�mero " + valor2 + " est� fora do intervalo.");
		}
		if (valor3 < 10 | valor3 > 500) {
			System.out.println("O n�mero " + valor3 + " est� fora do intervalo.");
		}

	}

}
